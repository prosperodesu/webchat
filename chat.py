import tornado.ioloop
import tornado.web
from webchat.forms import MessageForm
from tornado import autoreload
from tornado import websocket
from webchat.models import Message


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('templates/index.html')

class SocketHandler(websocket.WebSocketHandler):

    def check_origin(self, origin):
        return True

    def open(self):
        print 'Open socket'

    def on_message(self, message):
        print 'You are write %s' % message
        return message

    def on_close(self):
        print 'Socket Closed'



application = tornado.web.Application([
    (r"/", IndexHandler),
    (r"/socket", SocketHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    ioloop = tornado.ioloop.IOLoop.instance().start()
    autoreload.start(ioloop)
    ioloop.start()
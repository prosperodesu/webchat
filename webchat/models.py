from motorengine import Document
from motorengine.fields import *

class Message(Document):
    author = StringField()
    message = StringField(max_length=250)
    created = DateTimeField(auto_now_on_insert=True)

class Channel(Document):
    author = StringField()
    title = StringField(max_length=100)
    private = BooleanField(default=False)
    created = DateTimeField(auto_now_on_insert=True)
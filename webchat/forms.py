from wtforms.fields import *
from wtforms.validators import DataRequired
from wtforms import Form


class MessageForm(Form):
    message = TextAreaField(validators=[DataRequired(message='Input message!')])